# labwrat's user-overrides.js for Firefox

## IMPORTANT

**this user-overrides.js is a supplement intended to be appended to the [ghacksuserjs user.js file](https://github.com/ghacksuserjs/ghacks-user.js)** (a security and privacy centric user preferences file for Firefox and its derivatives) and used in conjunction with the ['Firefox Configuration Guide for Privacy Freaks and Performance Buffs'](https://12bytes.org/articles/tech/firefox/firefoxgecko-configuration-guide-for-privacy-and-performance-buffs) or [The Firefox Privacy Guide For Dummies!](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies)

the preferences in the user-overrides.js are my personal preferences which are not edited for public consumption, therefore you will need to adjust as necessary

## IMPLEMENTATION

1) read ['Firefox Configuration Guide for Privacy Freaks and Performance Buffs'](https://12bytes.org/articles/tech/firefox/firefoxgecko-configuration-guide-for-privacy-and-performance-buffs) or [The Firefox Privacy Guide For Dummies!](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies)
2) add the appropriate files to your Firefox profile from the [ghacksuserjs/ghacks-user.js repo](https://github.com/ghacksuserjs/ghacks-user.js), including the their user.js, updater.sh (Linux) or updater.bat (Windows), and prefsCleaner.sh (Linux) or prefsCleaner.bat (Windows) - don't forget to make the .sh scripts executable if you're running Linux
3) download the user-overrides.js file to your Firefox profile directory
4) edit the settings in user-overrides.js to suit your personal preferences - instructions are contained in the file
5) run the updater.sh/updater.bat file - for Linux, in a terminal run:
./updater.sh

## UPDATE NOTIFICATIONS

subscribe to the 'Firefox' category at [12bytes.org/subscribe](https://12bytes.org/subscribe) and/or to the [Activity feed](https://gitlab.com/labwrat/Firefox-user.js.atom) for this repository